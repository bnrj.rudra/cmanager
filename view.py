import gi
import requests
import urllib.parse as lurl
from urllib.request import urlopen, urlretrieve
import ast,pickle
import os, json
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, GdkPixbuf, Gio, Gdk
import sqlite3
import sysvars


class TreeViewer():
  def __init__(self):
    try:
      movie_list = []
      self.movie_liststore = Gtk.ListStore(str, str, str)
      movie_all = self.data_retrive()
      for i in range(len(movie_all)):
        movie_list.append(movie_all[i][0:3])

      for movie_ref in movie_list:
        self.movie_liststore.append(list(movie_ref))
        #Creating the filter, feeding it with the liststore model
        self.language_filter = self.movie_liststore.filter_new()
        #setting the filter function, note that we're not using the

      self.treeview = Gtk.TreeView.new_with_model(self.language_filter)
      for i, column_title in enumerate(["Title", "Year", "Language"]):
        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn(column_title, renderer, text=i)
        self.treeview.append_column(column)
    except:
      pass

  def data_insert(self, dblist):
    database = sysvars.datafile
    conn = sqlite3.connect(database)
    c = conn.cursor()
    c.execute('''CREATE TABLE IF NOT EXISTS Movies
             (ID INT PRIMARY KEY, Title text, Language text, Year text, Released text, Runtime text,
              Genre text, Director text, Writer text, Production text, Plot text,
              File text)''')
    c.executemany("INSERT INTO Movies VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", [dblist])
    conn.commit()
    conn.close()

  def data_retrive(self):
    database = sysvars.datafile
    conn = sqlite3.connect(database)
    c = conn.cursor()
    c.execute('SELECT * FROM Movies')
    movie_list = c.fetchall()
    conn.close()
    return movie_list


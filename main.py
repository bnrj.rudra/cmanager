#!/usr/bin/env python3
import gi
import requests
import urllib.parse as lurl
from urllib.request import urlopen, urlretrieve
import ast,pickle
import os, sys
import json
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, GdkPixbuf, Gio, Gdk
import subprocess, sqlite3
# Other Files
import preferences
import dialogue
import view
import apisource
import sysvars
#  basedir = os.path.join(os.path.expanduser("~"),".local/share/cmanage")
#  global datafile
#  datafile=os.path.join(basedir,"cmanage.db")
#  imagefile=os.path.join(basedir,".images")

class GridWindow(Gtk.ApplicationWindow):
  def __init__(self):
    Gtk.Window.__init__(self,
                        #  title="Collection Manager",
                        default_width=1000,
                        default_height=200,
                        border_width=5)



    # Files to store data
    self.basedir = sysvars.basedir #os.path.join(os.path.expanduser("~"),".local/share/cmanage")
    os.makedirs(self.basedir, exist_ok=True)
    self.data=sysvars.datafile # os.path.join(self.basedir,"cmanage.db")
    os.makedirs(os.path.join(self.basedir,".images"), exist_ok=True)
    self.images=sysvars.imagefile #(os.path.join(self.basedir,".images"))


    # Import other files
    self.Preferences = preferences.file_manager()
    self.Files = dialogue.FileDialog()
    self.Dialog = dialogue.MessageDialog()
    self.View = view.TreeViewer()
    self.Movies = apisource.Movies()
    #  self.API  =apisource.Movies()
    try:
      self.View.data_retrive()
    except:   #Movie db has not been created yer
      pass

    #  Decorations
    # Create headerbar
    self.headerbar = Gtk.HeaderBar()
    self.set_titlebar(self.headerbar)
    self.headerbar.set_show_close_button(True)
    # global main_header
    self.main_header = "Collection Manager"
    self.headerbar.set_title(self.main_header)
		# Menu using Gio
    h_grid = Gtk.Grid()
    FileButton = Gtk.MenuButton()
    EditButton = Gtk.MenuButton(label=None, image=Gtk.Image(icon_name="list-add-symbolic"))
    FileButton.props.label = "File"
    filemenu = Gio.Menu()
    editmenu = Gio.Menu()
    filemenu.append("Open", "win.open")
    #  filemenu.append("Save As", "win.save-as")
    #  filemenu.append("Save", "win.save")
    filemenu.append("About", "win.about")
    filemenu.append("Quit", "app.quit")
    editmenu.append("Import", "win.import")
    h_grid.attach(FileButton, 0, 0, 3, 1)
    h_grid.attach(EditButton, 3, 0, 1, 1)
    FileButton.set_menu_model(filemenu)
    EditButton.set_menu_model(editmenu)
    self.headerbar.pack_start(h_grid)
    # headerbar.pack_start(EditButton)

    about_action = Gio.SimpleAction.new("about", None)
    about_action.connect("activate", self.Dialog.about_activated)
    self.add_action(about_action)

    # accelgroup=Gtk.AccelGroup()
    # self.add_accel_group(accelgroup)

    # Menu (Stable)
    action = Gio.SimpleAction(name="save-as")
    #  action.connect("activate", self.file_save_as_clicked)
    #  self.add_action(action)

    # Open menu
    open_action = Gio.SimpleAction(name="open")
    open_action.connect("activate", self.file_open_clicked)
    self.add_action(open_action)

    action = Gio.SimpleAction(name="edit")
    #  action.connect("activate", self.MenuElem.create_textview)
    #  self.add_action(action)

    action = Gio.SimpleAction(name="save")
    #  action.connect("activate", self.file_save_clicked)
    #  self.add_action(action)

    action = Gio.SimpleAction(name="import")
    #  action = Gio.SimpleAction(name="edit")
    action.connect("activate", self.file_open_clicked)
    self.add_action(action)

    #  action.connect("activate", self.MenuElem.import_format)
    #  self.add_action(action)



    self.grid = Gtk.Grid()
    self.grid.set_column_spacing(1)
    self.add(self.grid)
    self.grid.set_baseline_row(1000)

    # Search box
    self.Keys = ["Title", "Year", "Language", "Released", "Runtime", "Genre", "Director", "Writer",
                "Country", "Production", "Plot", "Files", "Poster"]
    #  self.Keys = self.API.omdb_keys()
    self.all_keys =dict()
    self.movie_file = None
    bname = Gtk.Button(image=Gtk.Image(icon_name="find-symbolic"))
    bname.connect("clicked", self.on_search_click)
    self.badd = Gtk.Button(image=Gtk.Image(icon_name="list-add-symbolic"))
    self.badd.set_sensitive(False)
    self.badd.connect("clicked", self.on_add_click)

    # Add  the info eitries
    # Try it in a loop
    for i in range(len(self.Keys[:-2])):
      label = "l"+self.Keys[i]
      label = Gtk.Label(label=self.Keys[i])
      entry = "self.e"+self.Keys[i]
      label.set_xalign(Gtk.Justification.LEFT)
      if i < len(self.Keys[:-3]):
        self.all_keys[i] = Gtk.Entry()
        self.all_keys[i].set_placeholder_text(self.Keys[i])
        self.grid.attach(label, 555, i*15, 1, 1)
        self.grid.attach(self.all_keys[i], 560, i*15, 1, 1)
      if i == len(self.Keys[:-3]):
        self.all_keys[i] = Gtk.TextView()
        self.tbuff=self.all_keys[i].get_buffer()
        self.tbuff.set_text("Plot")
        self.grid.attach(label, 580, 0, 1, 1)
        self.grid.attach(self.all_keys[i], 580, 1, 400, 120)
        self.all_keys[i].set_wrap_mode(Gtk.WrapMode.WORD)



    # Add button to add entry
    self.grid.attach(self.badd, 300,180,700,1)
    self.grid.attach(bname, 570,0,1,1)
    # Add movie poster
    decframe = Gtk.Frame()
    self.simage = Gtk.Image.new_from_icon_name("folder-pictures-symbolic",1024)
    decframe.add(self.simage)
    self.grid.attach(decframe, 300,0, 250,140)
    # Add Play button
    self.bplay = Gtk.Button(image=Gtk.Image(icon_name="media-playback-symbolic"))
    self.bplay.connect("clicked", self.on_demand_play)
    self.bplay2 = Gtk.Button(image=Gtk.Image(icon_name="folder-add-symbolic"))
    self.bplay.set_sensitive(False)
    self.grid.attach(self.bplay, 980,0,2, 2)
    self.bplay.set_sensitive(False)
    self.grid.attach(self.bplay2, 980,15,2, 2)
    self.bplay2.set_sensitive(False)

    #  ListStore
    self.list =Gtk.ScrolledWindow()
    self.list.set_shadow_type(3)
    self.list.set_vexpand(True)
    self.grid.attach(self.list,0, 0,260,260)
    try:
      self.list.add(self.View.treeview)
    except:
      pass

    try:
      # On click on list
      self.View.treeview.get_selection().connect("changed", self.on_view_clicked)
    except AttributeError:
      pass

  def on_search_click(self, button):
    self.mname = self.all_keys[0].get_text()
    url = ("http://www.omdbapi.com/?t="+lurl.quote_plus(self.mname)+"&apikey=8e34211f")
    print(url)
    res = ast.literal_eval(urlopen(url).read().decode())
    try:
      for i in range(len(self.Keys[0:])):
        if i< len(self.Keys[:-3]):
          self.all_keys[i].set_text(res.get(self.Keys[i]))
      self.tbuff.set_text(res.get(self.Keys[-3]))
      pixname = os.path.join(self.images,res.get(self.Keys[0])+".jpg")
      urlretrieve(res.get("Poster"), pixname)
      self.pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_scale(filename=pixname, width=250, height=250,
                                                            preserve_aspect_ratio=True)
      self.simage.set_from_pixbuf(self.pixbuf)
      self.badd.set_sensitive(True)
      self.bplay2.set_sensitive(True)
    except TypeError:
      self.Dialog.on_error_clicked(
      "Movie "+self.mname+" not found", "Search another database")


  def on_add_click(self,button):
    vals = []
    for i in range(len(self.Keys[1:-2])):
      if i< len(self.Keys[:-2]):
        vals.append(self.all_keys[i].get_text())
    txt = self.all_keys[10].get_buffer()
    siter, eiter = txt.get_bounds()
    btxt = txt.get_text(siter, eiter, False)
    vals.append(btxt)
    try:
      vals.append(self.filename)
    except AttributeError:
      vals.append(None)
    self.View.data_insert(vals)

  def file_open_clicked(self, name, action):
    self.Files.FileChooser(["Open Existing Cmanage File",
                           "All Files", "*", True],
                           Gtk.FileChooserAction.OPEN, Gtk.STOCK_OPEN)
    if self.Files.response == Gtk.ResponseType.OK:
      self.filename = self.Files.dialog.get_filename()
      self.Files.dialog.destroy()
      print("Database "+self.filename+" selected")
      fname = (os.path.splitext(os.path.basename(self.filename))[0:-1][0])
      self.on_search_file(fname)
    elif self.Files.response == Gtk.ResponseType.CANCEL:
    	self.Files.dialog.destroy()

  def on_search_file(self, fname):
    self.mname = fname
    url = ("http://www.omdbapi.com/?t="+lurl.quote_plus(self.mname)+"&apikey=8e34211f")
    print(url)
    res = ast.literal_eval(urlopen(url).read().decode())
    try:
      for i in range(len(self.Keys[0:])):
        if i< len(self.Keys[:-3]):
          self.all_keys[i].set_text(res.get(self.Keys[i]))
      self.tbuff.set_text(res.get(self.Keys[-3]))
      pixname = os.path.join(self.images,res.get(self.Keys[0])+".jpg")
      urlretrieve(res.get("Poster"), pixname)
      self.pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_scale(filename=pixname, width=250, height=250,
                                                            preserve_aspect_ratio=True)
      self.simage.set_from_pixbuf(self.pixbuf)
      self.badd.set_sensitive(True)
    except TypeError:
      self.Dialog.on_error_clicked(
      "Movie "+self.mname+" not found", "Search another database")
    except URLError:
      self.Dialog.on_error_clicked(
        "Can not connect to Internet", "Check your connection")

  def on_demand_play(self, button):
    if os.path.isfile(self.movie_file):
      subprocess.run(["xdg-open",self.movie_file])
    else:
      self.Dialog.on_error_clicked("No such file of directory", "Check your file system")


  def on_view_clicked(self, selection):
    (model, iter) = selection.get_selected()
    dbfile = sysvars.datafile
    conn = sqlite3.connect(dbfile)
    c = conn.cursor()
    c.execute("SELECT * FROM Movies WHERE ID=?", (model[iter][0],))
    for row in c:
      rr=((row))
    for i in range(len(rr)-2):
      self.all_keys[i].set_text(rr[i])
    pixname = os.path.join(sysvars.imagefile, model[iter][0]+".jpg")
    self.pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_scale(filename=pixname, width=250, height=250,
                                                          preserve_aspect_ratio=True)
    self.simage.set_from_pixbuf(self.pixbuf)
    self.tbuff.set_text(rr[-2])
    self.movie_file = rr[-1]
    #  print(rr[-1])
    if self.movie_file is not None:
      self.bplay.set_sensitive(True)
      self.bplay2.set_sensitive(False)
    elif self.movie_file is None:
      self.bplay.set_sensitive(False)
      self.bplay2.set_sensitive(True)


#  class cman(Gtk.Application):

#      def __init__(self):
#          Gtk.Application.__init__(self)
#          #  self.Messages = dialogue.MessageDialog()
#          #  self.do_activate()

#      def do_activate(self):
#          win = GridWindow(self)
#          win.show_all()

#      def do_startup(self):
#          Gtk.Application.do_startup(self)
#          action = Gio.SimpleAction(name="utility")
#          #  action.connect("activate", self.Messages.utility_activated)
#          self.add_action(action)
#          action = Gio.SimpleAction(name="settings")
#          #  action.connect("activate", self.Messages.settings)
#          self.add_action(action)
#          action = Gio.SimpleAction(name="about")
#          #  action.connect("activate", self.Messages.about_activated)
#          self.add_action(action)
#          action = Gio.SimpleAction(name="quit")
#          action.connect("activate", lambda a, b: self.quit())
#          self.add_action(action)


#  def main(version=""):
#    app = cman()
#    r = app.run(sys.argv)
#    sys.exit(r)

#  if __name__ == "__main__":
#    main()



win = GridWindow()
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()
